/**
 * Created by Елена on 05.03.2017.
 */
'use strict';
(function(url, saveUrl){
    var dropdown = document.querySelector('.b-autocomplete-dropdown'),
        inputAuto = document.getElementById('autocomplete'),
        inputPhone = document.getElementById('tel-number'),
        message = document.getElementsByClassName('message')[0],
        form = document.querySelector(".b-form"),
        textMessage = {
            '1': 'Ошибка! Пожалуйста заполните все поля',
            '2': 'Ошибка сервера',
            '3': 'Значения сохранены',
            '4': 'Значения очищены'
        };

    function getXmlHttp(){ //Cross-browser compatibility XMLHttpRequest
        var xmlhttp;
        try {
            xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            try {
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (E) {
                xmlhttp = false;
            }
        }
        if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
            xmlhttp = new XMLHttpRequest();
        }
        return xmlhttp;
    }

    function sendArrRequests(str){
        var countElem= 0;
        dropdown.innerHTML = '';
        if(url.length > 0){
            url.forEach(function(val){
                    var xhr = getXmlHttp();
                    //if you have server search, you must write val+='?search='+str and don't call function searchStr
                    xhr.open('GET', val, true);
                    xhr.send();
                    xhr.onreadystatechange = function() {
                        if (xhr.readyState !== XMLHttpRequest.DONE) return;
                        if(xhr.status == 200){
                            var arrNames = JSON.parse(xhr.responseText);
                            countElem = searchStr(arrNames, str, countElem);
                            if(!document.querySelector('.b-autocomplete-dropdown li')){
                                dropdown.classList.remove("active");
                            }
                        }else{
                            showMessage(2, 'error');
                        }
                    };
            });
        }
    }

    function saveRequest(result){
        if(saveUrl){
            var xhr = getXmlHttp();
            xhr.open("POST", saveUrl, true);
            xhr.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
            xhr.send(result);
            xhr.onreadystatechange = function (){
                if (xhr.readyState !== XMLHttpRequest.DONE) return;
                if(xhr.status == 200){
                    //..
                }else{
                    showMessage(2, 'error');
                }
            }
        }
    }

    function searchStr(arrNames, str, countElem){
        for(var i=0; i < arrNames.length; i++){
            if((arrNames[i].name.indexOf(str) + 1) && (countElem < 4)) {
                dropdown.innerHTML += '<li>' + arrNames[i].name + '</li>';
                countElem++;
            }
            if(!dropdown.classList.contains("active")){
                dropdown.className += " active";
            }
            if(countElem >= 4){
                break;
            }
        }
        return countElem;
    }

    function showMessage(num, textClass){
        message.innerHTML = textMessage[num];
        message.className = textClass + "-message";
        message.style.display = "block";
        setTimeout(function(){
            message.style.display = 'none';
        },3000);
    }

    function validateForm(){
        if(inputAuto.value !== '' && inputPhone.value !== ''){
            message.style.display = 'none';
            localStorage.setItem('auto', inputAuto.value);
            localStorage.setItem('phone', inputPhone.value);
            saveRequest();
            showMessage(3, 'success');
        }else{
            showMessage(1, 'error');
        }
    }

    function clearForm(){
        delete localStorage['auto'];
        delete localStorage['phone'];
        inputAuto.value = '';
        inputPhone.value = '';
        showMessage(4, 'success');
    }

    // page
    var autoValue = localStorage.getItem('auto'),
        phoneValue = localStorage.getItem('phone');
    if(autoValue){
        inputAuto.value = autoValue;
    }
    if(phoneValue){
        inputPhone.value = phoneValue;
    }

    document.onclick = function(e){
        var elem = e.target,
            parent = elem.closest('.b-autocomplete-dropdown');
        if(elem.tagName == 'LI' && parent){
            document.getElementById('autocomplete').value = elem.textContent;
            parent.className = parent.className.replace( /(?:^|\s)active(?!\S)/ , '' );
        }else if(elem.className == 'btn-save'){
            validateForm();
        }else if(elem.className == 'btn-clear'){
            clearForm();
        }
    };

    document.getElementById('autocomplete').onkeydown = function(e){
        sendArrRequests(this.value + e.key);
    };

    form.addEventListener("submit", function(event) {
        event.preventDefault();
    });

})(arrSuggestUrl, saveUrl);
